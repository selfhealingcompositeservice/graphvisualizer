package fr.dauphine.graphvisualizer;

import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFrame;

import thirdparty.ScreenImage;
import edu.uci.ics.jung.algorithms.layout.KKLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import fr.dauphine.graphreader.GraphReader;

/**
* This class contains method regarding the visualization of graphs.
* @author Rafael Angarita
* @version 1.0
*/

public class GraphVisualizer {
	
	private static String imageFilename;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	static public void visualizeGraph(Graph<Integer, Number> graph) throws FileNotFoundException, UnsupportedEncodingException{
		Layout<Integer, String> layout = new KKLayout(graph);
        layout.setSize(new Dimension(1000,1000)); // sets the initial size of the layout space
        // The BasicVisualizationServer<V,E> is parameterized by the vertex and edge types
        BasicVisualizationServer<Integer,String> vv = new BasicVisualizationServer<Integer,String>(layout);
        vv.setPreferredSize(new Dimension(1000,1000)); //Sets the viewing area size
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());//show vertex label
        
        JFrame frame = new JFrame("Simple Graph View");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(vv); 
        frame.pack();
        //frame.setVisible(true);
        writeToImageFile(frame);
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
	}
	
	static private void writeToImageFile(JFrame frame) {

		   BufferedImage bufImage = ScreenImage.createImage((JComponent) frame.getComponent(0));
		   try {
		       File outFile = new File(imageFilename);
		       ImageIO.write(bufImage, "png", outFile);
		   } catch (Exception e) {
		       e.printStackTrace();
		   }
		}
	
	public void visualizeGraph(String filename) {
		imageFilename = trimFileExtension(filename) + ".png";
		
		try {
			visualizeGraph(GraphReader.getGraph(filename));
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
	}
	
	private String trimFileExtension(String filename) {
		
		if(filename.indexOf(".") != -1)
			return filename.substring(0, filename.lastIndexOf('.'));
		return filename;


	}

	public static void main(String[] args) {
		
		(new GraphVisualizer()).visualizeGraph(args[0]);

	}
}